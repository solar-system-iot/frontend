import { useThree } from "react-three-fiber";
import { CubeTextureLoader } from "three";
import space_1 from "../../assets/space_1.jpg";
import space_2 from "../../assets/space_2.jpg";
import space_3 from "../../assets/space_3.jpg";
import space_4 from "../../assets/space_4.jpg";
// Loads the skybox texture and applies it to the scene.
const SkyBox = () => {
  const { scene } = useThree();
  const loader = new CubeTextureLoader();
  // The CubeTextureLoader load method takes an array of urls representing all 6 sides of the cube.
  const texture = loader.load(
    [space_1, space_2, space_3, space_4, space_1, space_2],
    (texture) => {
      scene.background = texture;
    }
  );

  // Set the scene background property to the resulting texture.
  scene.background = texture;
  return null;
};

export default SkyBox;
