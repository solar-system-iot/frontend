import React, { useEffect, useMemo, useRef } from "react";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { useFrame, useLoader } from "react-three-fiber";
const Sphere = ({ model }) => {
  const planet = useRef();
  const { nodes } = useLoader(GLTFLoader, model);
  const t = Object.keys(nodes)[1];
  const geom = useMemo(() => nodes[t].geometry, [model]);
  const mat = useMemo(() => nodes[t].material, [model]);
  useFrame(() => (planet.current.rotation.y += 0.0002));

  return (
    <mesh
      ref={planet}
      visible
      position={[0, 0, 0]}
      scale={[1, 1, 1]}
      // Adding data from mars.glb to the geometry and material of the sphere
      geometry={geom}
      material={mat}
    />
  );
};

export default Sphere;
