import { Canvas } from "@react-three/fiber";

import { CameraControls, Sphere, SkyBox } from "./index";
import { Suspense } from "react";
import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles(() => ({
  canvas: {
    margin: "0",
    padding: "0",
    position: "absolute !important",
    top: "0",
    zIndex: "0",
  },
}));

function PlanetModel({ model }) {
  const classes = useStyle();
  return (
    <Canvas className={classes.canvas}>
      <CameraControls />
      <directionalLight intensity={1} />
      <ambientLight intensity={0.6} />
      <Suspense fallback="loading">
        <Sphere model={model} />
      </Suspense>
      <SkyBox />
    </Canvas>
  );
}

export default PlanetModel;
