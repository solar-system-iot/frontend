import {
  Button,
  ClickAwayListener,
  Grow,
  MenuItem,
  MenuList,
  Paper,
  Popper,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { useHistory } from "react-router";
import solar_icon from "../assets/solar-system-icon.png";
import { PLANET_NAME } from "../const";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    background: "#000",
    zIndex: 2000,
    position: "sticky",
    top: 0,
    justifyContent: "center",
    "& .MuiAppBar-colorPrimary": {
      backgroundColor: "transparent",
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  button: {
    marginLeft: "auto",
  },
}));

export default function Header({ open, setOpen }) {
  const classes = useStyles();
  const history = useHistory();
  const anchorRef = React.useRef(null);
  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <Button
        ref={anchorRef}
        aria-owns={open ? "menu-list-grow" : null}
        aria-haspopup="true"
        onClick={() => setOpen(!open)}
        style={{ width: "256px", color: "#FFF" }}
      >
        <img
          src={solar_icon}
          style={{ width: "50px", height: "50px", marginRight: "16px" }}
          alt={"solar"}
        />
        Solar system
      </Button>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        transition
        disablePortal
        style={{ width: "256px", zIndex: "2000" }}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            id="menu-list-grow"
            style={{
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList>
                  {PLANET_NAME.map(({ label, icon }) => (
                    <MenuItem
                      onClick={() => {
                        history.push(`/${label}`);
                      }}
                    >
                      <img
                        src={icon}
                        alt={label}
                        style={{
                          width: "50px",
                          height: "50px",
                          marginRight: "16px",
                        }}
                      />
                      {label}
                    </MenuItem>
                  ))}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
}
