import { Backdrop, Box } from "@material-ui/core";
import React from "react";
import loading from "../assets/loading.gif";

const Loading = ({ isLoading, setIsLoading }) => {
  return (
    <Box
      position="fixed"
      width="100%"
      height="100vh"
      display={"flex"}
      justifyContent={"center"}
      alignItems={"center"}
      zIndex={3000}
    >
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isLoading}
        onClick={setIsLoading}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <img src={loading} width={"600px"} height={"500px"} alt={"loading"} />
        <h1>Loading . . .</h1>
      </Backdrop>
    </Box>
  );
};

export default Loading;
