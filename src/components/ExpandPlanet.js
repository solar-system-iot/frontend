import { Box, Button, makeStyles, Typography } from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import React from "react";
import { useHistory } from "react-router";

const useStyle = makeStyles(() => ({
  "@keyframes floatUp": {
    "0%": {
      transform: "translateY(30%)",
    },
    "100%": {
      transform: "translateY(20%)",
    },
  },
  title: {
    color: "#FFFF",
    fontSize: "200px",
    fontWeight: "bold",
  },
  paragraph: {
    // padding: "32px",
    // fontSize: "24px",
  },
  planet: {
    width: "2000px",
    height: "2000px",
    transform: "translateY(20%)",
    animation: `$floatUp 6s linear`,
  },
  dataTitle: {
    fontSize: "14px",
  },
  dataContent: {
    fontWeight: "100",
  },
  button: {
    background: "linear-gradient(45deg, #0052A2 30%, #000B18 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 48,
    padding: "0 30px",
    boxShadow: "0 3px 5px 2px #02386E",
  },
  infoBox: {
    textAlign: "center",
  },
}));

const ExpandPlanet = ({ img, name, sol, temp, pressure, today, radius }) => {
  const classes = useStyle();
  const history = useHistory();

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="space-around"
    >
      <Box
        zIndex="1000"
        marginBottom="32px"
        display="flex"
        height="500px"
        alignItems="center"
        justifyContent="flex-end"
      >
        <Box width="50%" paddingX="48px">
          <Typography variant="h1" className={classes.title}>
            {name}
          </Typography>
          <Box width="80%">
            <Typography variant="body1" className={classes.paragraph}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
              mollis vulputate purus, sit amet placerat lorem viverra eu. Donec
              at magna iaculis dolor consectetur vulputate. Suspendisse ornare
              fringilla nisi eget semper. Ut in diam bibendum, iaculis libero
              at, feugiat nunc. Duis imperdiet ut enim sed luctus. Curabitur
              sollicitudin, sem ut rhoncus commodo, ipsum quam accumsan urna, id
              pharetra lacus lectus eget leo. Mauris feugiat, ligula vel
              accumsan sodales, est eros condimentum mi, nec accumsan mi ligula
              vel massa. Aliquam commodo est id nisi fringilla dictum a eget
              purus. Nulla convallis imperdiet orci luctus mollis. Donec
              imperdiet venenatis lectus non viverra.
            </Typography>
          </Box>
        </Box>
        <Box width="30%">
          <Button
            style={{
              color: "#FFFF",
            }}
            className={classes.button}
            endIcon={<ArrowForwardIcon />}
            onClick={() => history.push("/solar")}
          >
          EXPLORE
          </Button>
        </Box>
      </Box>
      <Box
        width="1000px"
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        zIndex="1000"
      >
        <Box className={classes.infoBox}>
          <Typography variant="h6" className={classes.dataTitle}>
            DAYS
          </Typography>
          <Typography variant="h5" className={classes.dataContent}>
            {today}
          </Typography>
        </Box>
        <Box className={classes.infoBox}>
          <Typography variant="h6" className={classes.dataTitle}>
            RADIUS(km)
          </Typography>
          <Typography variant="h5" className={classes.dataContent}>
            {radius}
          </Typography>
        </Box>
        <Box className={classes.infoBox}>
          <Typography variant="h6" className={classes.dataTitle}>
            SURVEY DAY(Day)
          </Typography>
          <Typography variant="h5" className={classes.dataContent}>
            {sol}
          </Typography>
        </Box>
        <Box className={classes.infoBox}>
          <Typography variant="h6" className={classes.dataTitle}>
            CURRENT TEMPERATURE(°C)
          </Typography>
          <Typography variant="h5" className={classes.dataContent}>
            {temp}
          </Typography>
        </Box>
        <Box className={classes.infoBox}>
          <Typography variant="h6" className={classes.dataTitle}>
            CURRENT PRESSURE(Pa)
          </Typography>
          <Typography variant="h5" className={classes.dataContent}>
            {pressure}
          </Typography>
        </Box>
      </Box>
      <Box position="fixed" height="100vh">
        <img src={img} className={classes.planet} alt={name} />
      </Box>
    </Box>
  );
};

export default ExpandPlanet;
