import React, { useEffect, useState } from "react";
import { Box, Card, makeStyles, Paper, Slide } from "@material-ui/core";
import { timeNumber } from "../const";

const useStyle = makeStyles(() => ({
  root: {
    fontSize: "64px",
    fontWeight: "700",
    textAlign: "center",
    color: "#FFFF",
  },
}));

const CountDownCard = ({ number }) => {
  const classes = useStyle();

  return <Box className={classes.root}>{number}</Box>;
};

export default CountDownCard;
