import { Box, Typography } from "@material-ui/core";
import React, { useEffect, useMemo, useState } from "react";
import CountDownCard from "./CountDownCard";

const currentDate = new Date();

const CountDown = () => {
  const [year, setYear] = useState(2300);
  const [month, setMonth] = useState(8);
  const [day, setDay] = useState(20);
  const [hour, setHour] = useState(23);
  const [minute, setMinute] = useState(53);
  const [second, setSecond] = useState(20);
  const [date, setDate] = useState(
    new Date(year, month, day, hour, minute, second)
  );
  const y = date.getFullYear() - currentDate.getFullYear();
  const m = date.getMonth();
  const d = date.getDate();
  const h = date.getHours();
  const mi = date.getMinutes();
  const s = date.getSeconds();
  useEffect(() => {
    const interval = setInterval(() => {
      const d = date.getSeconds() - 1;
      const x = new Date(date.setSeconds(d));
      setDate(x);
      return () => clearInterval(interval);
    }, 1000);
  }, []);

  return (
    <Box>
      <Box display="flex" marginBottom="16px" alignItems="center">
        <Box>
          <Box display="flex">
            {y < 10 ? (
              <>
                <CountDownCard number={0} />
                <CountDownCard number={0} />
              </>
            ) : (
              y < 100 && <CountDownCard number={0} />
            )}
            {y
              .toString()
              .split("")
              .map((num) => (
                <CountDownCard number={num} />
              ))}
          </Box>
          <Box>Years</Box>
        </Box>
        <Typography variant="h3">:</Typography>
        <Box>
          <Box display="flex">
            {m < 10 && <CountDownCard number={0} />}
            {m
              .toString()
              .split("")
              .map((num) => (
                <CountDownCard number={num} />
              ))}
          </Box>
          <Box>Months</Box>
        </Box>
        <Typography variant="h3">:</Typography>
        <Box>
          <Box display="flex">
            {d < 10 && <CountDownCard number={0} />}
            {d
              .toString()
              .split("")
              .map((num) => (
                <CountDownCard number={num} />
              ))}
          </Box>
          <Box>Days</Box>
        </Box>
        <Typography variant="h3">:</Typography>
        <Box>
          <Box display="flex">
            {h < 10 && <CountDownCard number={0} />}
            {h
              .toString()
              .split("")
              .map((num) => (
                <CountDownCard number={num} />
              ))}
          </Box>
          <Box>Hours</Box>
        </Box>
        <Typography variant="h3">:</Typography>
        <Box>
          <Box display="flex">
            {mi < 10 && <CountDownCard number={0} />}
            {mi
              .toString()
              .split("")
              .map((num) => (
                <CountDownCard number={num} />
              ))}
          </Box>
          <Box>Minutes</Box>
        </Box>
        <Typography variant="h3">:</Typography>
        <Box>
          <Box display="flex">
            {s < 10 && <CountDownCard number={0} />}
            {s
              .toString()
              .split("")
              .map((num) => (
                <CountDownCard number={num} />
              ))}
          </Box>
          <Box>Seconds</Box>
        </Box>
      </Box>
    </Box>
  );
};

export default CountDown;
