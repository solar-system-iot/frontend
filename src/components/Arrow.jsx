import { Box, makeStyles } from "@material-ui/core";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

const useStyle = makeStyles(() => ({
  arrow: {
    display: "flex",
    alignItems: "center",
  },
  transform: {
    transform: " rotate(180deg)",
  },
}));

const Arrow = ({ direction, clickFunction }) => {
  const classes = useStyle();

  return (
    <Box className={classes.arrow} textAlign="center" onClick={clickFunction}>
      <Box>
        <ArrowForwardIosIcon
          className={direction === "left" ? classes.transform : ""}
        />
      </Box>
    </Box>
  );
};

export default Arrow;
