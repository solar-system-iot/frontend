import { Box, makeStyles, Typography } from "@material-ui/core";
import React from "react";

const useStyle = makeStyles(() => ({
  "@keyframes floatUp": {
    "0%": {
      transform: "translateY(30%)",
    },
    "100%": {
      transform: "translateY(20%)",
    },
  },
  title: {
    color: "#FFFF",
    fontSize: "200px",
    fontWeight: "bold",
  },
  paragraph: {
    // padding: "32px",
    // fontSize: "24px",
  },
  planet: {
    width: "2000px",
    height: "2000px",
    transform: "translateY(20%)",
    animation: `$floatUp 6s linear`,
  },
  dataTitle: {
    fontSize: "14px",
  },
  dataContent: {
    fontWeight: "100",
  },
  button: {
    background: "linear-gradient(45deg, #0052A2 30%, #000B18 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 48,
    padding: "0 30px",
    boxShadow: "0 3px 5px 2px #02386E",
  },
  infoBox: {
    textAlign: "center",
  },
}));

const TodayData = ({ today, radius, sol, temp, pressure }) => {
  const classes = useStyle();

  return (
    <Box
      width="1000px"
      display="flex"
      justifyContent="space-around"
      alignItems="center"
      zIndex="1000"
    >
      <Box className={classes.infoBox}>
        <Typography variant="h6" className={classes.dataTitle}>
          DAYS
        </Typography>
        <Typography variant="h5" className={classes.dataContent}>
          {today}
        </Typography>
      </Box>
      <Box className={classes.infoBox}>
        <Typography variant="h6" className={classes.dataTitle}>
          RADIUS(km)
        </Typography>
        <Typography variant="h5" className={classes.dataContent}>
          {radius}
        </Typography>
      </Box>
      <Box className={classes.infoBox}>
        <Typography variant="h6" className={classes.dataTitle}>
          SURVEY DAY(Day)
        </Typography>
        <Typography variant="h5" className={classes.dataContent}>
          {sol}
        </Typography>
      </Box>
      <Box className={classes.infoBox}>
        <Typography variant="h6" className={classes.dataTitle}>
          CURRENT TEMPERATURE(°C)
        </Typography>
        <Typography variant="h5" className={classes.dataContent}>
          {temp.toFixed(2)}
        </Typography>
      </Box>
      <Box className={classes.infoBox}>
        <Typography variant="h6" className={classes.dataTitle}>
          CURRENT PRESSURE(Pa)
        </Typography>
        <Typography variant="h5" className={classes.dataContent}>
          {pressure.toFixed(2)}
        </Typography>
      </Box>
    </Box>
  );
};

export default TodayData;
