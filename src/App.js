import { Box } from "@material-ui/core";
import Router from "./router";
import "./App.css";

function App() {
  return (
    <Box>
      <Router />
    </Box>
  );
}

export default App;
