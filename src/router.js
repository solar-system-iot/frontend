import { Box } from "@material-ui/core";
import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import HomePage from "./pages/home/index";
import {
  MarsContainer,
  VenusContainer,
  EarthContainer,
  KeplerContainer,
} from "./pages/solar/index";

export default function Router() {
  const [open, setOpen] = useState(false);

  return (
    <Box width="100%" height="100%">
      {/* <Header /> */}
      <Header open={open} setOpen={setOpen} />
      <Switch>
        <Route path="/Mars">
          <MarsContainer />
        </Route>
        <Route path="/Venus">
          <VenusContainer />
        </Route>
        <Route path="/Earth">
          <EarthContainer />
        </Route>
        <Route path="/Kepler">
          <KeplerContainer />
        </Route>
        <Route path="/">
          <HomePage />
        </Route>
      </Switch>
    </Box>
  );
}
