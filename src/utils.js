export const sortByDate = ({ array, by = "ASC" }) => {
  return array === undefined
    ? []
    : array.sort((a, b) =>
        by.toUpperCase() === "ASC"
          ? new Date(a.name).valueOf() - new Date(b.name).valueOf()
          : new Date(b.name).valueOf() - new Date(a.name).valueOf()
      );
};
