import { Box, makeStyles, MobileStepper, Slide } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Arrow from "../../../components/Arrow";
import {
  getAllTodayInfo,
  getPlanetInfoByName,
} from "../../../services/PlanetService";
import { formatData, TIME } from "../../solar/Utils/utils";
import { array } from "../Components/index";

const useStyle = makeStyles(() => ({
  root: {
    backgroundColor: "#000",
    height: "100vh",
    overflow: "hidden",
    position: "relative",
    color: "#FFFF",
  },
  stepper: {
    display: "flex",
    justifyContent: "center",
  },
}));

const HomeContainer = () => {
  const classes = useStyle();
  const [data, setData] = useState([]);
  const history = useHistory();
  const [index, setIndex] = useState(0);

  const [slideIn, setSlideIn] = useState(true);
  const [slideDirection, setSlideDirection] = useState("left");
  const [today, setToday] = useState("2020-07-05");
  const [isGet, setIsGet] = useState(false);
  const numSlides = array?.length ?? 0;

  const onArrowClick = (direction) => {
    const increment = direction === "left" ? -1 : 1;
    const newIndex = (index + increment + numSlides) % numSlides;

    const oppDirection = direction === "left" ? "right" : "left";
    setSlideDirection(direction === "left" ? "left" : "right");
    setSlideIn(false);
    setTimeout(() => {
      setIndex(newIndex);
      setSlideDirection(oppDirection);
      setSlideIn(true);
    }, 300);
  };

  useEffect(() => {
    if (isGet) {
      const timeId = setInterval(() => {
        getPlanetInfoByName({ planetName: "Mars" })
          .then((res) => {
            if (res) {
              const t = formatData(res);
              const to = t.get("temp")[t.get("temp")?.length - 1]?.name;
              setToday(to);
              return to;
            }
          })
          .then((to) => {
            if (to !== "" && to !== undefined) {
              getAllTodayInfo({ today: to })
                .then((data) => {
                  if (data) setData(data);
                })
                .catch((err) => console.log(err));
            }
          })
          .catch((err) => console.log(err));
      }, [TIME]);

      return () => clearInterval(timeId);
    } else {
      getPlanetInfoByName({ planetName: "Mars" })
        .then((res) => {
          if (res) {
            const t = formatData(res);
            const to = t.get("temp")[t.get("temp")?.length - 1]?.name;
            setToday(to);
            return to;
          }
        })
        .then((to) => {
          if (to !== "" && to !== undefined) {
            getAllTodayInfo({ today: to })
              .then((data) => {
                if (data) {
                  setData(data);
                  setIsGet(true);
                }
              })
              .catch((err) => console.log(err));
          }
        })
        .catch((err) => console.log(err));
    }
  }, [isGet]);
  return (
    <Box className={classes.root}>
      <Box
        height={"90%"}
        display={"flex"}
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Arrow direction="left" clickFunction={() => onArrowClick("left")} />
        <Slide direction={slideDirection} in={slideIn}>
          <Box width={"80%"}>
            {data.length > 0 &&
              array[index].render({
                data: data.filter(
                  ({ planetName }) => planetName === array[index].name
                ),
                history,
                today,
              })}
          </Box>
        </Slide>
        <Arrow direction="right" clickFunction={() => onArrowClick("right")} />
      </Box>
      <MobileStepper
        variant="dots"
        steps={array.length}
        position="bottom"
        activeStep={index}
        classes={{ root: classes.stepper }}
      />
    </Box>
  );
};

export default HomeContainer;
