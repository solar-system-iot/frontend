import { Box, Button, Typography } from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import React from "react";
import earth from "../../../assets/earth.png";
import TodayData from "../../../components/TodayData";
import { radius } from "../../../const";

const Earth = ({ data, history, today }) => {
  return (
    <Box>
      <Box display={"flex"}>
        <Box
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"flex-end"}
        >
          <Typography variant={"h1"}>Earth</Typography>
          <Typography variant={"p"} className={"paragraph"}>
            Our home planet Earth is a rocky, terrestrial planet. It has a solid
            and active surface with mountains, valleys, canyons, plains and so
            much more. Earth is special because it is an ocean planet. Water
            covers 70% of Earth's surface.
          </Typography>
          <Button
            style={{ color: "#fff", marginTop: "32px", marginBottom: "32px" }}
            className={"button"}
            endIcon={<ArrowForwardIcon />}
            onClick={() => history.push("/Earth")}
          >
            EXPLORE
          </Button>
        </Box>

        <img
          src={earth}
          style={{ width: "512px", height: "512px" }}
          alt={"earth"}
        />
      </Box>
      <TodayData
        sol={data.sol}
        temp={(data.min_temp + data.max_temp) / 2}
        today={today}
        pressure={data.pressure}
        radius={radius.EARTH}
      />
    </Box>
  );
};

export default Earth;
