import { Box, Button, Typography } from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import React from "react";
import mars from "../../../assets/Mars_.png";
import TodayData from "../../../components/TodayData";
import { radius } from "../../../const";

const Mars = ({ data, history, today }) => {
  return (
    <Box>
      <Box display={"flex"}>
        <Box
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"flex-end"}
        >
          <Typography variant={"h1"}>Mars</Typography>
          <Typography variant={"p"} className={"paragraph"}>
            Mars is very cold. The average temperature on Mars is minus 80
            degrees Fahrenheit -- way below freezing!
          </Typography>
          <Button
            style={{ color: "#fff", marginTop: "32px", marginBottom: "32px" }}
            className={"button"}
            endIcon={<ArrowForwardIcon />}
            onClick={() => history.push("/Mars")}
          >
            EXPLORE
          </Button>
        </Box>
        <img
          src={mars}
          style={{ width: "512px", height: "512px" }}
          alt={"earth"}
        />
      </Box>
      <TodayData
        sol={data.sol}
        temp={(data.min_temp + data.max_temp) / 2}
        today={today}
        pressure={data.pressure}
        radius={radius.MARS}
      />
    </Box>
  );
};

export default Mars;
