import { Box, Button, Typography } from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import React from "react";
import venus from "../../../assets/venus.png";
import TodayData from "../../../components/TodayData";
import { radius } from "../../../const";

const Venus = ({ data, history, today }) => {
  return (
    <Box>
      <Box display={"flex"}>
        <Box
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"flex-end"}
        >
          <Typography variant={"h1"}>Venus</Typography>
          <Typography variant={"p"} className={"paragraph"}>
            Venus is the second planet from the Sun, and is Earth's closest
            neighbor in the solar system. Venus is the brightest object in the
            sky after the Sun and the Moon, and sometimes looks like a bright
            star in the morning or evening sky. The planet is a little smaller
            than Earth, and is similar to Earth inside.
          </Typography>
          <Button
            style={{ color: "#fff", marginTop: "32px", marginBottom: "32px" }}
            className={"button"}
            endIcon={<ArrowForwardIcon />}
            onClick={() => history.push("/Venus")}
          >
            EXPLORE
          </Button>
        </Box>

        <img
          src={venus}
          style={{ width: "512px", height: "512px" }}
          alt={"venus"}
        />
      </Box>
      <TodayData
        sol={data.sol}
        temp={(data.min_temp + data.max_temp) / 2}
        today={today}
        pressure={data.pressure}
        radius={radius.VENUS}
      />
    </Box>
  );
};

export default Venus;
