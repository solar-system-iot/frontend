import Mars from "./Mars";
import Venus from "./Venus";
import Earth from "./Earth";
import Kepler from "./Kepler";

export const array = [
  {
    render: ({ data, history, today }) => (
      <Mars data={data[0]} history={history} today={today} />
    ),
    name: "Mars",
  },
  {
    render: ({ data, history, today }) => (
      <Venus data={data[0]} history={history} today={today} />
    ),
    name: "Venus",
  },
  {
    render: ({ data, history, today }) => (
      <Earth data={data[0]} history={history} today={today} />
    ),
    name: "Earth",
  },
  {
    render: ({ data, history, today }) => (
      <Kepler data={data[0]} history={history} today={today} />
    ),
    name: "Kepler-452 b",
  },
];
