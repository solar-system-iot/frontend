import { Box, Button, Typography } from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import React from "react";
import kepler from "../../../assets/Kepler-452b.png";
import TodayData from "../../../components/TodayData";
import { radius } from "../../../const";

const Kepler = ({ data, history, today }) => {
  return (
    <Box>
      <Box display={"flex"}>
        <Box
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"flex-end"}
        >
          <Typography variant={"h1"}>Kepler</Typography>
          <Typography variant={"p"} className={"paragraph"}>
            Kepler-452b is a super-Earth exoplanet orbiting within the inner
            edge of the habitable zone of the Sun-like star Kepler-452, and is
            the only planet in the system discovered by Kepler. It is located
            about 1,402 light-years from Earth in the constellation of Cygnus.
          </Typography>
          <Button
            style={{ color: "#fff", marginTop: "32px", marginBottom: "32px" }}
            className={"button"}
            endIcon={<ArrowForwardIcon />}
            onClick={() => history.push("/Kepler")}
          >
            EXPLORE
          </Button>
        </Box>

        <img
          src={kepler}
          style={{ width: "512px", height: "512px" }}
          alt={"earth"}
        />
      </Box>
      <TodayData
        sol={data.sol}
        temp={(data.min_temp + data.max_temp) / 2}
        today={today}
        pressure={data.pressure}
        radius={radius.KEPLER}
      />
    </Box>
  );
};

export default Kepler;
