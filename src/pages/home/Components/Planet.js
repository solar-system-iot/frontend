import React from "react";

const Planet = ({ img, name, className }) => {
  return <img src={img} alt="planet" className={className} />;
};

export default Planet;
