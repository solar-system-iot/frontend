import { ShowChart } from "@material-ui/icons";
import Barchart from "@material-ui/icons/BarChart";

export const TODAY = "2019-05-15";

export const TIME = 30000;

export const DATA_NAME = [
  "min_temp",
  "max_temp",
  "temp",
  "wind_speed",
  "pressure",
];

export const DATA_GRAPH_CONSTANT = {
  min_temp: {
    title: "Min temp",
    value: "min_temp",
    design: "line",
    yLabel: "°C",
    color: "#7D3AC1",
    canChangeColor: true,
    canChangeDesign: true,
  },
  max_temp: {
    title: "Max temp",
    value: "max_temp",
    design: "line",
    yLabel: "°C",
    color: "#EA7369",
    canChangeColor: true,
    canChangeDesign: true,
  },
  temp: {
    title: "Temperature",
    value: "temp",
    design: "line",
    yLabel: "°C",
    color: "#FCAF3B",
    canChangeColor: true,
    canChangeDesign: true,
  },
  pressure: {
    title: "Pressure",
    value: "pressure",
    design: "bar",
    yLabel: "Pa",
    color: "#DB4CB2",
    canChangeColor: true,
    canChangeDesign: true,
  },
  wind_speed: {
    title: "Wind speed",
    value: "wind_speed",
    design: "line",
    yLabel: "m/s",
    color: "#EAC",
    canChangeColor: true,
    canChangeDesign: true,
  },
};

export const formatData = (data) => {
  const m = new Map();

  for (let i = 0; i < data.length; i++) {
    const item = data[i];
    DATA_NAME.map((key) => {
      const date = item["terrestrial_date"].slice(
        0,
        item["terrestrial_date"].indexOf("T")
      );
      if (!m.get(key) && key !== "temp") {
        m.set(key, [{ name: date, value: Math.floor(item[key]) }]);
      } else if (key !== "temp") {
        const temp = m.get(key);
        temp.push({ name: date, value: Math.floor(item[key]) });
        m.set(key, temp);
      }

      if (!m.get(key) && key === "temp") {
        m.set(key, [
          {
            name: date,
            value:
              Math.floor(
                Math.floor(item["min_temp"]) + Math.floor(item["max_temp"])
              ) / 2,
            // value2: Math.floor(item["max_temp"]),
          },
        ]);
      } else if (key === "temp") {
        const temp = m.get(key);
        temp.push({
          name: date,
          value:
            Math.floor(
              Math.floor(item["min_temp"]) + Math.floor(item["max_temp"])
            ) / 2,
          // value2: Math.floor(item["max_temp"]),
        });
        m.set(key, temp);
      }
      return "";
    });
  }

  return m;
};

export const mergeData = (data1, data2) => {
  const m = new Map();
  Array.from(data1).map(([key, value]) => {
    m.set(key, []);
    const minLength = Math.min(data1.get(key).length, data2.get(key).length);
    for (let i = 0; i < minLength; i++) {
      const temp = m.get(key);
      temp.push({
        name: data1.get(key)[i].name,
        value1: Math.floor(data1.get(key)[i].value ?? 0),
        value2: Math.floor(data2.get(key)[i].value ?? 0),
      });
      m.set(key, temp);
    }
    return "";
  });
  return m;
};

export const GROUND_TITLE = {
  Al_g: "Aluminum",
  Ca_g: "Calcium",
  Cl_g: "Chlorine",
  Fe_g: "Iron",
  K_g: "Potassium",
  Mg_g: "Magnesium",
  Na_g: "Sodium",
  O_g: "Oxygen",
  Si_g: "Silicon",
  other_g: "Other",
};

export const GAS_TITLE = {
  Ar_a: "Argon",
  CO2_a: "Carbon dioxide",
  N_a: "Nitrogen",
  O_a: "Oxygen",
};

export const menuItem = Object.keys(DATA_GRAPH_CONSTANT).map((key) => {
  return { value: key, label: DATA_GRAPH_CONSTANT[key].title };
});

export const menuGraphItem = [
  {
    value: "line",
    Icon: ({ style }) => <ShowChart style={{ ...style }} />,
  },
  { value: "bar", Icon: ({ style }) => <Barchart style={{ ...style }} /> },
];
