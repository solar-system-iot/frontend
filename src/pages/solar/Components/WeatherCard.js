import { Box, makeStyles, Typography } from "@material-ui/core";
import WinterIcon from "@material-ui/icons/AcUnit";
import AutumnIcon from "@material-ui/icons/Eco";
import SpringIcon from "@material-ui/icons/LocalFlorist";
import SummerIcon from "@material-ui/icons/WbSunny";
import React from "react";
import autumn_bg from "../../../assets/autumn_bg.jfif";
import spring_bg from "../../../assets/spring_bg.jfif";
import sunny_bg from "../../../assets/sunny_bg.jfif";
import winter_bg from "../../../assets/winter_bg.jfif";
import { TODAY } from "../Utils/utils";

const useStyle = makeStyles({
  root: (props) => ({
    width: "100%",
    height: "100%",
    display: "flex",
    padding: "16px",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    background: `url(${props.bg}) no-repeat center center`,
    borderRadius: "8px",
    color: props.color,
    // textStroke: "2px #000",
  }),
  icon: {
    "& div > * ": {
      width: "128px",
      height: "128px",
    },
  },
  titleDate: {
    fontWeight: "bold",
    fontSize: "36px",
  },
  subtitleDate: {
    fontSize: "24px",
  },
  temp: {
    fontSize: "64px",
    fontWeight: "bold",
  },
});

const WEATHERS = {
  AUTUMN: <AutumnIcon style={{ width: "150px", height: "150px" }} />,
  SUMMER: <SummerIcon style={{ width: "150px", height: "150px" }} />,
  SPRING: <SpringIcon style={{ width: "150px", height: "150px" }} />,
  WINTER: <WinterIcon style={{ width: "150px", height: "150px" }} />,
};

const getIcon = (ls) => {
  switch (parseInt(ls / 90)) {
    case 0:
      return { bg: sunny_bg, icon: WEATHERS.SUMMER };

    case 1:
      return { bg: winter_bg, icon: WEATHERS.WINTER, color: "#000" };

    case 2:
      return { bg: spring_bg, icon: WEATHERS.SPRING, color: "#000" };

    default: {
      return { bg: autumn_bg, icon: WEATHERS.AUTUMN };
    }
  }
};

const WeatherCard = ({ value, ls, today }) => {
  const weather = getIcon(ls);
  const classes = useStyle(weather);
  const currentDate = new Date(today);
  return (
    <Box className={classes.root}>
      <Box textAlign="left" width="100%">
        <Typography variant="h5" className={classes.titleDate}>
          {currentDate.toLocaleDateString("en-EN", { weekday: "long" })}
        </Typography>
        <Typography variant="h5" className={classes.subtitleDate}>
          {currentDate.toLocaleDateString("en-EN", {
            year: "numeric",
            month: "long",
            day: "numeric",
          })}
        </Typography>
        <Typography variant="h5" className={classes.subtitleDate}>
          @ Mars
        </Typography>
      </Box>
      <Box className={classes.icon}>{weather.icon}</Box>
      <Typography variant="h3" className={classes.temp}>
        {value} °C
      </Typography>
    </Box>
  );
};

export default WeatherCard;
