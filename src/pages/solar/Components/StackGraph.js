import { makeStyles } from "@material-ui/core";
import React from "react";
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Area,
  AreaChart,
  ResponsiveContainer,
  Brush,
} from "recharts";

const useStyle = makeStyles(() => ({
  tooltip: {
    background: "#FFFF",
    textAlign: "left",
    color: "#000",
    padding: "8px  ",
  },
  dataLabel: {
    color: "#8884d8",
    fontWeight: "bold",
  },
}));

const CustomTooltip = ({ active, payload, label }) => {
  const classes = useStyle();
  if (active && payload && payload.length) {
    return (
      <div className={classes.tooltip}>
        <p className={classes.dataLabel}>{`date : ${label}`}</p>
        <p className={classes.dataLabel}>{`min : ${payload[0].value}`}</p>
        <p className={classes.dataLabel}>{`max : ${payload[1].value}`}</p>
      </div>
    );
  }

  return null;
};

const StackGraph = ({ data, xLabel, yLabel, isSync, title, ...props }) => {
  return (
    data !== undefined && (
      <ResponsiveContainer width="95%" height="100%">
        <AreaChart
          data={data}
          syncId={isSync ? "0" : title}
          margin={{
            top: 10,
            right: 0,
            left: 0,
            bottom: 80,
          }}
          {...props}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" tick={{ fill: "#9ccddc", fontSize: "12px" }} />
          <YAxis
            type="number"
            tick={{ fill: "#9ccddc", fontSize: "12px" }}
            domain={["dataMin", "dataMax"]}
            allowDataOverflow={true}
          />
          <Tooltip content={<CustomTooltip />} />
          <Brush
            dataKey="name"
            height={20}
            stroke="green"
            startIndex={Math.floor(data.length / 1.1) ?? 1000}
          />
          <Area
            type="monotone"
            dataKey="min"
            stackId="1"
            stroke="#82ca9d"
            fill="#82ca9d"
          />
          <Area
            type="monotone"
            dataKey="max"
            stackId="1"
            stroke="#ffc658"
            fill="#ffc658"
          />
        </AreaChart>
      </ResponsiveContainer>
    )
  );
};

export default StackGraph;
