import React from "react";
import {
  Bar,
  BarChart,
  Brush,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

const BarComparedGraph = ({
  data,
  xLabel,
  yLabel,
  color,
  title,
  isSync,
  brushState,
  setBrushState,
  ...props
}) => {
  return (
    <ResponsiveContainer width="95%" height="100%">
      <BarChart
        data={data}
        syncId={isSync ? "0" : title}
        margin={{
          top: 10,
          right: 0,
          left: 0,
          bottom: 80,
        }}
        {...props}
      >
        <defs>
          <linearGradient id={color} x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor={color} stopOpacity={0.8} />
            <stop offset="95%" stopColor={color} stopOpacity={0.2} />
          </linearGradient>
        </defs>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" tick={{ fill: "#9ccddc", fontSize: "12px" }} />
        <YAxis
          tick={{
            fill: "#9ccddc",
            fontSize: "12px",
            useCallback: (label) => {
              if (Math.floor(label) === label) return label;
            },
          }}
          domain={["auto", (dataMax) => dataMax * 1.5]}
          allowDecimals={false}
        />
        <Brush
          dataKey="name"
          height={20}
          stroke="green"
          startIndex={brushState}
          onChange={(state) => {
            setBrushState(state.startIndex);
          }}
        />
        <Tooltip />
        {/* <Line type="monotone" dataKey="value1" stroke="#8884d8" />
        <Line type="monotone" dataKey="value2" stroke="#82ca9d" /> */}
        <Bar dataKey="value1" fill="#8884d8" />
        <Bar dataKey="value2" fill="#82ca9d" />
      </BarChart>
    </ResponsiveContainer>
  );
};

export default BarComparedGraph;
