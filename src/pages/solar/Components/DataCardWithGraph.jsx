import React, { useState } from "react";
import DataCard from "./DataCard";

const DataCardWithGraph = ({
  render,
  title,
  yLabel,
  color,
  canSelected,
  selectedValue,
  setSelectedValue,
  canChangeColor,
  canChangeDesign,
  dataLength,
  design,
  isCompared,
}) => {
  const [currentColor, setColor] = useState(color);
  const [brushState, setBrushState] = useState(Math.floor(dataLength / 1.01));
  const [graphSelected, setGraphSelected] = useState(design);
  return (
    <DataCard
      title={`${title}(${yLabel})`}
      canChangeColor={canChangeColor}
      setColor={setColor}
      color={currentColor}
      canSelected={canSelected}
      selectedValue={selectedValue}
      setSelectedValue={setSelectedValue}
      canChangeDesign={canChangeDesign}
      graphSelected={graphSelected}
      setGraphSelected={setGraphSelected}
      isCompared={isCompared}
    >
      {render({
        color: currentColor,
        brushState,
        setBrushState,
        graphSelected,
        setGraphSelected,
      })}
    </DataCard>
  );
};

export default DataCardWithGraph;
