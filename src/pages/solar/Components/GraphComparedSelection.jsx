import React from "react";
import BarComparedGraph from "./BarComparedGraph";
import LineComparedGraph from "./LineComparedGraph";

const GRAPH_DESIGN = [
  {
    design: "line",
    render: ({
      value,
      isSync,
      yLabel,
      color,
      title,
      brushState,
      setBrushState,
    }) => (
      <LineComparedGraph
        data={value}
        isSync={isSync}
        yLabel={yLabel}
        color={color}
        title={title}
        brushState={brushState}
        setBrushState={setBrushState}
      />
    ),
  },
  {
    design: "bar",
    render: ({
      value,
      isSync,
      yLabel,
      color,
      title,
      brushState,
      setBrushState,
    }) => (
      <BarComparedGraph
        data={value}
        isSync={isSync}
        yLabel={yLabel}
        color={color}
        title={title}
        brushState={brushState}
        setBrushState={setBrushState}
      />
    ),
  },
];

const GraphComparedSelection = ({
  value,
  isSync,
  yLabel,
  color,
  title,
  brushState,
  setBrushState,
  graphSelected,
}) => {
  return (
    graphSelected !== undefined &&
    GRAPH_DESIGN.filter(({ design }) =>
      design.includes(graphSelected)
    )[0].render({
      value,
      isSync,
      yLabel,
      color,
      title,
      brushState,
      setBrushState,
    })
  );
};

export default GraphComparedSelection;
