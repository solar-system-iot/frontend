import {
  Box,
  Divider,
  Grow,
  Popper,
  ClickAwayListener,
  MenuList,
  MenuItem,
  Grid,
} from "@material-ui/core";
import React, { useState, useRef } from "react";
import DropdownFilter from "./DropDownFilter";
import { menuItem, menuGraphItem } from "../Utils/utils";
import { SketchPicker } from "react-color";
import PaletteIcon from "@material-ui/icons/Palette";
import BarChart from "@material-ui/icons/BarChart";

const DataCard = ({
  title,
  canSelected,
  selectedValue,
  setSelectedValue,
  canChangeDesign,
  canChangeColor,
  children,
  style,
  setColor,
  color,
  graphSelected,
  setGraphSelected,
  isCompared,
}) => {
  const [open, setOpen] = useState(false);
  const [openGraph, setOpenGraph] = useState(false);
  const anchorRef = useRef();
  const anchorGraphRef = useRef();
  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  const handleCloseGraph = (event) => {
    if (
      anchorGraphRef.current &&
      anchorGraphRef.current.contains(event.target)
    ) {
      return;
    }
    setOpenGraph(false);
  };

  return (
    <Box
      height="100%"
      bgcolor="#4C4C4C"
      style={{ background: "rgba(85, 145, 169,.5)", ...style }}
      zIndex="100"
      borderRadius={"8px"}
    >
      <Box
        paddingY={1}
        paddingX={2}
        display={"flex"}
        justifyContent={"space-between"}
        alignItems={"center"}
      >
        {!canSelected && title}
        {canSelected && (
          <DropdownFilter
            title={title}
            selectedValue={selectedValue}
            setSelectedValue={setSelectedValue}
            menuItem={menuItem}
            canSelected={canSelected}
            style={{ background: "transparent" }}
          />
        )}
        <Box>
          {canChangeDesign && (
            <BarChart
              onClick={() => setOpenGraph(!openGraph)}
              style={{ cursor: "pointer" }}
              ref={anchorGraphRef}
            />
          )}
          {!isCompared && canChangeColor && (
            <PaletteIcon
              onClick={() => setOpen(!open)}
              style={{ cursor: "pointer" }}
              ref={anchorRef}
            />
          )}
        </Box>
        <Popper
          open={openGraph}
          anchorEl={anchorGraphRef.current}
          transition
          disablePortal
          style={{ zIndex: "2000" }}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom",
              }}
            >
              <ClickAwayListener onClickAway={handleCloseGraph}>
                <MenuList>
                  <Grid container style={{ background: "#FFF" }}>
                    {menuGraphItem.map(({ value, Icon }) => (
                      <Grid item xs={6}>
                        <MenuItem onClick={() => setGraphSelected(value)}>
                          {Icon({ style: { color: "#000" } })}
                        </MenuItem>
                      </Grid>
                    ))}
                  </Grid>
                </MenuList>
              </ClickAwayListener>
            </Grow>
          )}
        </Popper>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          transition
          disablePortal
          style={{ zIndex: "2000" }}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom",
              }}
            >
              <ClickAwayListener onClickAway={handleClose}>
                <SketchPicker
                  color={color}
                  onChange={(color) => {
                    setColor(color.hex);
                  }}
                />
              </ClickAwayListener>
            </Grow>
          )}
        </Popper>
      </Box>
      <Divider style={{ width: "100%" }} />
      {children}
    </Box>
  );
};

export default DataCard;
