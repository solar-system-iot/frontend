import { makeStyles } from "@material-ui/core";
import React from "react";
import {
  Bar,
  BarChart,
  Brush,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

const useStyle = makeStyles(() => ({
  tooltip: {
    background: "#FFFF",
    textAlign: "left",
    color: "#000",
    padding: "8px  ",
    borderRadius: "8px",
  },
  dataLabel: {
    color: "#8884d8",
    fontWeight: "bold",
  },
}));

const CustomTooltip = ({ active, payload, label, y1, y2 }) => {
  const classes = useStyle();
  if (active && payload && payload.length) {
    return (
      <div className={classes.tooltip}>
        <p className={classes.dataLabel}>{`date : ${label}`}</p>
        <p className={classes.dataLabel}>{`value : ${payload[0].value}`}</p>
      </div>
    );
  }

  return null;
};

const BarGraph = ({
  data,
  xLabel,
  yLabel,
  color,
  title,
  isSync,
  brushState,
  setBrushState,
  ...props
}) => {
  return (
    <ResponsiveContainer width="95%" height="100%">
      <BarChart
        data={data}
        syncId={isSync ? "0" : title}
        margin={{
          top: 10,
          right: 0,
          left: 0,
          bottom: 80,
        }}
        {...props}
      >
        <defs>
          <linearGradient id={color} x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor={color} stopOpacity={0.8} />
            <stop offset="95%" stopColor={color} stopOpacity={0.2} />
          </linearGradient>
        </defs>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" tick={{ fill: "#9ccddc", fontSize: "12px" }} />
        <YAxis
          tick={{
            fill: "#9ccddc",
            fontSize: "12px",
            useCallback: (label) => {
              if (Math.floor(label) === label) return label;
            },
          }}
          domain={["dataMin", "dataMax"]}
          allowDecimals={false}
        />
        <Brush
          dataKey="name"
          height={20}
          stroke="green"
          startIndex={brushState}
          onChange={(state) => {
            setBrushState(state.startIndex);
          }}
        />
        <Tooltip content={<CustomTooltip />} />

        <Bar dataKey="value" fill={`url(#${color})`} />
      </BarChart>
    </ResponsiveContainer>
  );
};

export default BarGraph;
