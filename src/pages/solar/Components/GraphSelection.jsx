import React from "react";
import BarGraph from "./BarGraph";
import LineGraph from "./LineGraph";

const GRAPH_DESIGN = [
  {
    design: "line",
    render: ({
      value,
      isSync,
      yLabel,
      color,
      title,
      brushState,
      setBrushState,
    }) => (
      <LineGraph
        data={value}
        isSync={isSync}
        yLabel={yLabel}
        color={color}
        title={title}
        brushState={brushState}
        setBrushState={setBrushState}
      />
    ),
  },
  {
    design: "bar",
    render: ({
      value,
      isSync,
      yLabel,
      color,
      title,
      brushState,
      setBrushState,
    }) => (
      <BarGraph
        data={value}
        isSync={isSync}
        yLabel={yLabel}
        color={color}
        title={title}
        brushState={brushState}
        setBrushState={setBrushState}
      />
    ),
  },
];

const GraphSelection = ({
  value,
  isSync,
  yLabel,
  color,
  title,
  brushState,
  setBrushState,
  graphSelected,
}) => {
  return (
    graphSelected !== undefined &&
    GRAPH_DESIGN.filter(({ design }) =>
      design.includes(graphSelected)
    )[0].render({
      value,
      isSync,
      yLabel,
      color,
      title,
      brushState,
      setBrushState,
    })
  );
};

export default GraphSelection;
