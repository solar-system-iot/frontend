import React from "react";
import GaugeChart from "react-gauge-chart";
import { Box } from "@material-ui/core";

const Meter = ({ value, unit, min, max }) => {
  return (
    <Box textAlign="center" height="100%">
      <Box height="auto">
        <GaugeChart
          id="gauge-chart1"
          colors={["#9ccddc", "#5591a9", "#054569"]}
          nrOfLevels={30}
          // arcPadding={0.1}
          cornerRadius={3}
          percent={(value - min) / (max - min)}
          formatTextValue={() => value?.toFixed(2) + " " + unit}
        />
      </Box>
    </Box>
  );
};

export default Meter;
