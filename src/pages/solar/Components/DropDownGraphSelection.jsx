import { Grid, MenuItem, MenuList } from "@material-ui/core";
import React from "react";

const DropDownGraphSelection = ({
  selectedValue,
  setSelectedValue,
  menuItem,
  canSelected,
  title,

  ...props
}) => {
  return (
    <MenuList>
      <Grid container style={{ width: "256px", background: "#FFF" }}>
        {menuItem.map(({ value, Icon }) => (
          <Grid item xs={3}>
            <MenuItem onClick={() => setSelectedValue(value)}>
              {Icon({ style: { color: "#000" } })}
              {value}
            </MenuItem>
          </Grid>
        ))}
      </Grid>
    </MenuList>
  );
};

export default DropDownGraphSelection;
