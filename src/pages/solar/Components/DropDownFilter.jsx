import { FormControl, makeStyles, MenuItem, Select } from "@material-ui/core";
import React from "react";
import { useLocation } from "react-router";

const useStyle = makeStyles(() => ({
  input: {
    "& .MuiSelect-select": {
      background: "#FFFF",
      borderRadius: "8px",
    },
  },
  dataCardSelected: {
    "& .MuiSelect-select": {
      background: "transparent",
      color: "#FFF",
      borderRadius: "8px",
      borderColor: "transparent",
    },
  },
}));

const DropdownFilter = ({
  selectedValue,
  setSelectedValue,
  menuItem,
  canSelected,
  title,
  ...props
}) => {
  const classes = useStyle();
  const location = useLocation();
  return (
    <FormControl size="small" style={{ width: "100%", padding: "8px" }}>
      <Select
        variant="outlined"
        value={selectedValue}
        onChange={({ target: { value } }) => {
          if (typeof value === "string") {
            setSelectedValue(value);
          }
        }}
        renderValue={(selected) => {
          return selected;
        }}
        style={{ minWidth: "100px", padding: "0px", color: "#000" }}
        className={canSelected ? classes.dataCardSelected : classes.input}
      >
        <MenuItem disabled value="">
          <em>compare with . . .</em>
        </MenuItem>
        <MenuItem value="None">None</MenuItem>
        {menuItem
          .filter(({ label }) => !location.pathname.includes(label))
          .map(({ value, label, icon }) => (
            <MenuItem value={value} key={label}>
              {icon && (
                <img
                  src={icon}
                  alt={label}
                  style={{
                    width: "50px",
                    height: "50px",
                    marginRight: "16px",
                  }}
                />
              )}
              {label}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default DropdownFilter;
