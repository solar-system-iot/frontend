import { Box, Switch } from "@material-ui/core";
import React, { useState } from "react";
import styled from "styled-components";
import { PLANET_NAME } from "../../../const";
import DataCardWithGraph from "../Components/DataCardWithGraph";
import DropdownFilter from "../Components/DropDownFilter";
import GraphComparedSelection from "../Components/GraphComparedSelection";
import GraphSelection from "../Components/GraphSelection";
import WeatherCard from "../Components/WeatherCard";
import { DATA_GRAPH_CONSTANT, menuItem } from "../Utils/utils";

const BoxContent = styled(Box)`
  padding: 8px;
`;

const DashBoardSection = ({
  todayData,
  dataset,
  shouldCompare,
  comparedDataset,
  comparedValue,
  setComparedValue,
  today,
}) => {
  const [selectedValue, setSelectedValue] = useState("pressure");
  const [isSync, setIsSync] = useState(false);

  return (
    <Box height="100%">
      <Box width="512px" display="flex" alignItems={"center"}>
        <DropdownFilter
          selectedValue={selectedValue}
          setSelectedValue={setSelectedValue}
          menuItem={menuItem}
        />
        <DropdownFilter
          selectedValue={comparedValue}
          setSelectedValue={setComparedValue}
          menuItem={PLANET_NAME}
        />
        <Switch checked={isSync} onChange={() => setIsSync(!isSync)} />
      </Box>
      <Box display="flex" height="512px" width="100%" marginBottom="8px">
        {shouldCompare
          ? Array.from(comparedDataset)
              .filter(([key, value]) => key === selectedValue)
              .map(([key, value]) => {
                const isCompared = true;
                return (
                  <BoxContent width="80%" padding="8px">
                    <DataCardWithGraph
                      isCompared={isCompared}
                      render={({
                        color,
                        brushState,
                        setBrushState,
                        graphSelected,
                        setGraphSelected,
                      }) => {
                        return (
                          <>
                            <GraphComparedSelection
                              value={value}
                              isSync={isSync}
                              yLabel={DATA_GRAPH_CONSTANT[key].yLabel}
                              color={color}
                              title={DATA_GRAPH_CONSTANT[key].title}
                              brushState={brushState}
                              setBrushState={setBrushState}
                              graphSelected={graphSelected}
                              setGraphSelected={setGraphSelected}
                            />
                          </>
                        );
                      }}
                      {...DATA_GRAPH_CONSTANT[key]}
                      dataLength={value.length}
                    />
                  </BoxContent>
                );
              })
          : Array.from(dataset)
              .filter(([key, value]) => key === selectedValue)
              .map(([key, value]) => {
                return (
                  <BoxContent width="80%" padding="8px">
                    <DataCardWithGraph
                      isCompared={false}
                      render={({
                        color,
                        brushState,
                        setBrushState,
                        graphSelected,
                        setGraphSelected,
                      }) => {
                        return (
                          <>
                            <GraphSelection
                              value={value}
                              isSync={isSync}
                              yLabel={DATA_GRAPH_CONSTANT[key].yLabel}
                              color={color}
                              title={DATA_GRAPH_CONSTANT[key].title}
                              brushState={brushState}
                              setBrushState={setBrushState}
                              graphSelected={graphSelected}
                              setGraphSelected={setGraphSelected}
                            />
                          </>
                        );
                      }}
                      {...DATA_GRAPH_CONSTANT[key]}
                      dataLength={value.length}
                    />
                  </BoxContent>
                );
              })}
        <BoxContent width="20%" padding="8px">
          <WeatherCard
            today={today}
            value={((todayData.max_temp + todayData.min_temp) / 2).toFixed(2)}
            ls={todayData?.ls}
          />
        </BoxContent>
      </Box>
      <Box display="flex" height="240px" width="100%">
        {Array.from(dataset).map(([key, value], index) => (
          <BoxContent width="80%" padding="8px">
            <DataCardWithGraph
              isCompared={false}
              render={({
                color,
                brushState,
                setBrushState,
                graphSelected,
                setGraphSelected,
              }) => {
                return (
                  <>
                    <GraphSelection
                      value={value}
                      isSync={isSync}
                      yLabel={DATA_GRAPH_CONSTANT[key].yLabel}
                      color={color}
                      title={DATA_GRAPH_CONSTANT[key].title + index}
                      brushState={brushState}
                      setBrushState={setBrushState}
                      graphSelected={graphSelected}
                      setGraphSelected={setGraphSelected}
                    />
                  </>
                );
              }}
              {...DATA_GRAPH_CONSTANT[key]}
              dataLength={value.length}
            />
          </BoxContent>
        ))}
      </Box>
    </Box>
  );
};

export default DashBoardSection;
