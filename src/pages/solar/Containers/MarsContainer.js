import { Box, makeStyles, Typography } from "@material-ui/core";
import React, { useEffect, useMemo, useState } from "react";
import mars_back from "../../../assets/mars_back.jpg";
import mars from "../../../assets/planets/mars.glb";
import Loading from "../../../components/Loading";
import { PLANET_NAME_ONLY } from "../../../const";
import {
  getPlanetInfoByName,
  getTodayInfo,
} from "../../../services/PlanetService";
import {
  formatData,
  GAS_TITLE,
  GROUND_TITLE,
  mergeData,
  TIME,
} from "../Utils/utils";
import DashBoardSection from "./DashBoardSection";
import TemperatureSection from "./TemperatureSection";

const useStyle = makeStyles(() => ({
  root: {
    backgroundColor: "#000",
    minHeight: "100vh",
    position: "relative",
    color: "#FFFF",
    textAlign: "center",
  },
  title: {
    color: "#FFFF",
    fontSize: "200px",
    fontWeight: "bold",
    marginBottom: "32px",
  },
  subtitle: {
    color: "#FFFF",
    fontSize: "64px",
    fontWeight: "bold",
    marginBottom: "32px",
  },
  timer: {
    background: `url(${mars_back}) no-repeat center center fixed`,
    backgroundSize: "cover",
    height: "100vh",
  },
  temperatureSection: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
    position: "relative",
    padding: "16px",
    boxSizing: "border-box",
    margin: "0 auto",
  },
  dashboardSection: {
    backgroundColor: "#062c43",
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
    position: "relative",
    padding: "32px",
    boxSizing: "border-box",
    margin: "0 auto",
  },
  pressureSection: {
    backgroundColor: "#FFFF",
    display: "flex",
    height: "500px",
    padding: "128px",
  },
}));

const MarsContainer = () => {
  const classes = useStyle();
  const [dataset, setDataSet] = useState(new Map());
  const [comparedDataset, setComparedDataset] = useState(new Map());
  const [comparedValue, setComparedValue] = useState("compare with . . .");
  const [isGetCompared, setIsGetCompared] = useState(false);

  const shouldCompare = useMemo(() => {
    setIsGetCompared(false);
    return PLANET_NAME_ONLY.includes(comparedValue);
  }, [comparedValue]);
  const [data, setData] = useState({
    sol: 0,
    ls: 0,
    min_temp: 0,
    max_temp: 0,
    terrestrial_date: new Date(),
    pressure: 0,
  });
  const [ground, setGround] = useState({});
  const [gas, setGas] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [isGet, setIsGet] = useState(false);
  const [today, setToday] = useState("2020-07-05");


  useEffect(() => {
    if (isGetCompared) {
      const timerId = setInterval(() => {
        getPlanetInfoByName({ planetName: comparedValue })
          .then((res) => {
            if (res) {
              const temp = formatData(res);
              return temp;
            }
          })
          .then((data) => {
            getPlanetInfoByName({ planetName: "Mars" })
              .then((res) => {
                if (res) {
                  const t = formatData(res);
                  const to = t.get("temp")[t.get("temp")?.length - 1]?.name;
                  setToday(to);
                  setComparedDataset(mergeData(t, data));
                  setIsLoading(false);
                }
                setIsLoading(false);
              })
              .catch((err) => console.log(err));
          })
          .catch((err) => console.log(err));
      }, TIME);

      return () => clearInterval(timerId);
    }
  }, [isGetCompared]);

  useEffect(() => {
    if (shouldCompare && !isGetCompared) {
      getPlanetInfoByName({ planetName: comparedValue })
        .then((res) => {
          if (res) {
            const temp = formatData(res);
            return temp;
          }
        })
        .then((data) => {
          setComparedDataset(mergeData(dataset, data));
          setIsLoading(false);
          setIsGetCompared(true);
        })
        .catch((err) => console.log(err));
    }
  }, [comparedValue]);

  useEffect(() => {
    getTodayInfo({ planet: "Mars" }).then((data) => {
      if (data && data.length > 0) {
        const temp = Object.keys(GROUND_TITLE).map((key) => {
          return { name: GROUND_TITLE[key], value: data[0][key] ?? 0 };
        });

        const temp2 = Object.keys(GAS_TITLE).map((key) => {
          return { name: GAS_TITLE[key], value: data[0][key] ?? 0 };
        });

        setGround(temp);
        setGas(temp2);
        setData(data[0]);
      }
    });
  }, []);

  useEffect(() => {
    if (isGet) {
      const timerId = setInterval(() => {
        getPlanetInfoByName({ planetName: "Mars" })
          .then((res) => {
            if (res) {
              setDataSet(formatData(res));
            }
            setIsLoading(false);
          })
          .catch((err) => console.log(err));
      }, TIME);

      return () => clearInterval(timerId);
    } else {
      getPlanetInfoByName({ planetName: "Mars" })
        .then((res) => {
          if (res) {
            const t = formatData(res);
            const to = t.get("temp")[t.get("temp")?.length - 1]?.name;
            setToday(to);
            setDataSet(t);
          }
          setIsLoading(false);
          setIsGet(true);
        })
        .catch((err) => console.log(err));
    }
  }, [isGet]);

  return (
    <Box className={classes.root}>
      {isLoading && (
        <Loading isLoading={isLoading} setIsLoading={setIsLoading} />
      )}
      <Box
        className={classes.timer}
        display={"flex"}
        justifyContent={"center"}
        alignItems={"center"}
      >
        <Typography variant="h1" className={classes.title}>
          MARS
        </Typography>
      </Box>
      <Box className={classes.temperatureSection}>
        {dataset.size > 0 && (
          <TemperatureSection
            todayData={data}
            dataset={dataset}
            model={mars}
            gas={gas}
            ground={ground}
          />
        )}
      </Box>
      <Box className={classes.dashboardSection}>
        {dataset.size > 0 && (
          <DashBoardSection
            todayData={data}
            today={today}
            dataset={dataset}
            shouldCompare={shouldCompare}
            comparedDataset={comparedDataset}
            comparedValue={comparedValue}
            setComparedValue={setComparedValue}
          />
        )}
      </Box>
    </Box>
  );
};

export default MarsContainer;
