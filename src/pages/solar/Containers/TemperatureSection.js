import { Box, makeStyles } from "@material-ui/core";
import React, { useState } from "react";
import styled from "styled-components";
import kepler from "../../../assets/Kepler-452b.png";
import PlanetModel from "../../../components/Model";
import DataCard from "../Components/DataCard";
import DataCardWithGraph from "../Components/DataCardWithGraph";
import GraphSelection from "../Components/GraphSelection";
import Meter from "../Components/Meter";
import MultiPieChart from "../Components/PieChart";
import { DATA_GRAPH_CONSTANT } from "../Utils/utils";

const Column = styled(Box)`
  width: "25%";
  z-index: 100;
`;
const Row = styled(Box)`
  display: flex;
  justify-content: space-between;
  height: 256px;
  width: 100%;
  margin-bottom: 16px;
`;

const useStyle = makeStyles(() => ({
  img: {
    position: "absolute",
    overflow: "hidden",
    width: "1500px",
    height: "1500px",
    left: "0",
  },
  deleteMargin: {
    "& div:last-child": {
      margin: "0px",
    },
  },
}));

const TemperatureSection = ({
  name,
  todayData,
  dataset,
  model,
  gas,
  ground,
}) => {
  const [selectedValue, setSelectedValue] = useState("pressure");
  const [selectedValueG2, setSelectedValueG2] = useState("wind_speed");
  const classes = useStyle();
  return (
    <Box position={"relative"}>
      {name !== "kepler" && <PlanetModel model={model} />}
      {name === "kepler" && (
        <img src={kepler} className={classes.img} alt={"kepler"} />
      )}
      <Row>
        <Column width="30%">
          <DataCard title={"Gas composition"}>
            <MultiPieChart data={gas} />
          </DataCard>
        </Column>
        <Column width="30%">
          <DataCard title={"Pressure (Pa)"}>
            {
              <Meter
                title={"Pressure"}
                value={todayData?.pressure}
                min={todayData?.min_pressure}
                max={todayData?.max_pressure}
                unit={"Pa"}
              />
            }
          </DataCard>
        </Column>
      </Row>
      <Row>
        <Column width="30%">
          <DataCard title={"Soil composition"}>
            <MultiPieChart data={ground} />
          </DataCard>
        </Column>
        <Column width="30%">
          <DataCard title={"Temperature (°C)"}>
            <Meter
              title={"Temperature"}
              value={todayData.avg_temp}
              unit={"°C"}
              min={todayData.all_min_t}
              max={todayData.all_max_t}
            />
          </DataCard>
        </Column>
      </Row>
      <Row className={classes.deleteMargin}>
        {Array.from(dataset)
          .filter(([key, value]) => key === selectedValue)
          .map(([key, value]) => {
            return (
              <Box zIndex={100} width={"30%"}>
                <DataCardWithGraph
                  canSelected={true}
                  selectedValue={selectedValue}
                  setSelectedValue={setSelectedValue}
                  render={({
                    color,
                    brushState,
                    setBrushState,
                    graphSelected,
                    setGraphSelected,
                  }) => {
                    return (
                      <>
                        <GraphSelection
                          value={value}
                          isSync={false}
                          yLabel={DATA_GRAPH_CONSTANT[key].yLabel}
                          color={color}
                          title={DATA_GRAPH_CONSTANT[key].title}
                          brushState={brushState}
                          setBrushState={setBrushState}
                          graphSelected={graphSelected}
                          setGraphSelected={setGraphSelected}
                        />
                      </>
                    );
                  }}
                  {...DATA_GRAPH_CONSTANT[key]}
                  dataLength={value.length}
                />
              </Box>
            );
          })}
        {Array.from(dataset)
          .filter(([key, value]) => key === selectedValueG2)
          .map(([key, value]) => {
            return (
              <Box zIndex={100} width={"30%"}>
                <DataCardWithGraph
                  canSelected={true}
                  selectedValue={selectedValueG2}
                  setSelectedValue={setSelectedValueG2}
                  render={({
                    color,
                    brushState,
                    setBrushState,
                    graphSelected,
                    setGraphSelected,
                  }) => {
                    return (
                      <>
                        <GraphSelection
                          value={value}
                          isSync={false}
                          yLabel={DATA_GRAPH_CONSTANT[key].yLabel}
                          color={color}
                          title={DATA_GRAPH_CONSTANT[key].title}
                          brushState={brushState}
                          setBrushState={setBrushState}
                          graphSelected={graphSelected}
                          setGraphSelected={setGraphSelected}
                        />
                      </>
                    );
                  }}
                  {...DATA_GRAPH_CONSTANT[key]}
                  dataLength={value.length}
                />
              </Box>
            );
          })}
      </Row>
    </Box>
  );
};

export default TemperatureSection;
