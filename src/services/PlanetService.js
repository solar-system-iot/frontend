import axios from "axios";
import { TODAY } from "../pages/solar/Utils/utils";

let HOST;

if (process.env.NODE_ENV === "development") {
  HOST = process.env.REACT_APP_HOST_DEVELOPMENT;
} else {
  HOST = process.env.REACT_APP_HOST;
}

export const getTodayInfo = async ({ planet, today }) => {
  // const today = new Date().toLocaleDateString("sv-SE");
  try {
    const response = await axios.get(
      `http://${HOST}/today?date=${TODAY}&name=${planet}`,
      {
        mode: "cors",
        headers: {
          "Content-type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    if (response.statusText) {
      return response.data;
    } else {
      return null;
    }
  } catch (err) {
    console.log(err);
  }
};

export const getAllTodayInfo = async ({ today }) => {
  // const today = new Date().toLocaleDateString("sv-SE");
  try {
    const response = await axios.get(`http://${HOST}/today?date=${today}`, {
      mode: "cors",
      headers: {
        "Content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    });
    if (response.statusText) {
      return response.data;
    } else {
      return null;
    }
  } catch (err) {
    console.log(err);
  }
};

export const getPlanetInfoByName = async ({ planetName }) => {
  try {
    const response = await axios.get(`http://${HOST}/planet/${planetName}`, {
      mode: "cors",
      headers: {
        "Content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    });
    if (response.statusText) {
      return response.data;
    } else {
      return null;
    }
  } catch (err) {
    console.log(err);
  }
};
