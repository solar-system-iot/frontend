import { sortByDate } from "./utils";
import mars_icon from "./assets/mars-icon.png";
import venus_icon from "./assets/venus-icon.png";
import earth_icon from "./assets/earth-icon.png";
import kepler_icon from "./assets/kepler-icon.png";

export const timeNumber = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
export const mass_info = { MARS: 6.39 };
export const gas_info = {
  MARS: [
    { name: "carbon", value: 0.95 },
    { name: "nitrogen", value: 0.01 },
    { name: "argon", value: 0.01 },
    { name: "oxygen", value: 0.01 },
  ],
};

export const PLANET_NAME = [
  { label: "Mars", value: "Mars", icon: mars_icon },
  { label: "Venus", value: "Venus", icon: venus_icon },
  { label: "Earth", value: "Earth", icon: earth_icon },
  { label: "Kepler", value: "Kepler-452 b", icon: kepler_icon },
];

export const PLANET_NAME_ONLY = ["Mars", "Venus", "Earth", "Kepler-452 b"];

export const radius = {
  MARS: 3389.5,
  VENUS: 6051.8,
  EARTH: 6371,
  KEPLER: 9556.5,
};

export const COLORS = [
  "#F95335",
  "#FCBCAA",
  "#50A3A4",
  "#FCAF3B",
  "#AABD8C",
  "#E43F6F",
  "#DFBBB1",
  "#B1E5F2",
  "#272635",
  "#F39B6D",
  "#3E6990",
];
let temperatureData = [];

for (let i = 0; i < 10; i++) {
  const t = Math.floor(Math.random() * 5) + 10;
  temperatureData.push({
    name: new Date(+new Date() - Math.floor(Math.random() * 10000000000)),
    min: t,
    max: t * Math.floor(Math.random() * 10),
  });
}

temperatureData = sortByDate({ array: temperatureData });
temperatureData = temperatureData.map((item) => {
  return {
    ...item,
    name: new Date(item.name).toLocaleDateString("en-GB"),
  };
});

let pressureData = [];

for (let i = 0; i < 50; i++) {
  pressureData.push({
    name: new Date(+new Date() - Math.floor(Math.random() * 10000000000)),
    value:
      Math.floor(Math.random() * 10) +
      Math.floor(Math.random() * 10) +
      Math.floor(Math.random() * 10),
  });
}

pressureData = sortByDate({ array: pressureData });
pressureData = pressureData.map((item) => {
  return {
    ...item,
    name: new Date(item.name).toLocaleDateString("en-GB"),
  };
});

let windSpeedData = [];

for (let i = 0; i < 50; i++) {
  windSpeedData.push({
    name: new Date(+new Date() - Math.floor(Math.random() * 10000000000)),
    value:
      Math.floor(Math.random() * 50) +
      Math.floor(Math.random() * 50) +
      Math.floor(Math.random() * 50),
  });
}
windSpeedData = sortByDate({ array: windSpeedData });
windSpeedData = windSpeedData.map((item) => {
  return {
    ...item,
    name: new Date(item.name).toLocaleDateString("en-GB"),
  };
});
const graphDesign = {
  LINE: "line",
  STACK: "STACK",
};

const dataset = [
  {
    title: "Temperature",
    design: "stack",
    data: temperatureData,
    yLabel: "°C",
  },
  {
    title: "Pressure",
    design: "line",
    data: pressureData,
    yLabel: "Pa",
    color: "#EA7369",
  },
  {
    title: "Wind speed",
    design: "line",
    data: windSpeedData,
    yLabel: "m/s",
    color: "#EAC",
  },
];

export { dataset, graphDesign, windSpeedData };
